 
var React = require('react');
var PropTypes = require('prop-types');  
    
class ForecastByCityName extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      cityName: '',
    };

    this.handleSubmitCityName = this.handleSubmitCityName.bind(this);
    this.handleUpdateCityName = this.handleUpdateCityName.bind(this);
  }
  handleSubmitCityName () {

    if (this.state.cityName.toString().trim().length != 0) {
       
      this.props.onSubmitCityName(this.state.cityName)

      this.setState(function () {
        return {
          cityName: ''
        }
      });
    }    
    
  }
  
  handleUpdateCityName (e) {
    var city = e.target.value;
    this.setState(function () {
      return {
        cityName: city
      }
    });
  }
  render() {
    return (
      <div
        className='cityName-container'
        style={{flexDirection: this.props.direction}}>
        <input 
          className={this.state.cityName.toString().trim().length==0 ? "form-control error" : "form-control"}
          
          onChange={this.handleUpdateCityName}
          placeholder='Inesrt city name..'
          type='text'
          value={this.state.cityName} />
        <button
          type='button'
          style={{margin: 10, width: '100%', background:'#0b64ad'}}
          className='btn btn-info'
          onClick={this.handleSubmitCityName}>
            Get Weather
        </button>
      </div>
    )
  }
}

ForecastByCityName.defaultProps = {
  direction: 'column'
}

ForecastByCityName.propTypes = {
  direction: PropTypes.string,
} 

module.exports = ForecastByCityName;