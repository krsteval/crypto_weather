var React = require('react');
var utils = require('../utils/helpers');
var convertTemp = require('../utils/helpers').convertTemp;
var getDate = utils.getDate;

function DayItem (props) {
  
  console.log(props.day.weather[0].icon);
  var date = getDate(props.day.dt);
  var icon = props.day.weather[0].icon;
  var minTemp = convertTemp( props.day.temp.min);
  console.log(props)
  var classSingleItem = props.classSingleItem == null ? 'dayContainer-cursor': props.classSingleItem;
  return (
    
    <div onClick={props.onClick} className={'col-lg-3 col-md-3 dayContainer ' + classSingleItem}>
      <img className='weather' src={'/app/images/weather-icons/' + icon + '.svg'} alt='Weather' />
      <h2 className='subheader'>{date}</h2>
      <h2 className='subheaderTemp'><b>{minTemp} °C</b></h2>
    </div>
  )
}

module.exports = DayItem;