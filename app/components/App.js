var React = require('react');
var ForecastByCityName = require('./ForecastByCityName');
var Forecast = require('./Forecast');
var Detail = require('./Detail');
var ReactRouter = require('react-router-dom');
var BrowserRouter = ReactRouter.BrowserRouter;
var Route = ReactRouter.Route;

class App extends React.Component {
  constructor(props) {
    super(props); 
  }
  
  render () {
    function handleClick() {
      
    }
    return (
      <BrowserRouter>
        <div className=''>
          <Route render={function (props) {
            return (
              <div className="navbar-container">
                <div className='container navbar'>
                  <h1 onClick={handleClick}>Crypto Weather</h1>
                  
                </div>
              </div>
            )
          }} />

          <Route exact path='/' render={function (props) {
            return (
              <div className='home-container'>
                <h1 className='header'>Enter a City</h1>
                <br/>
                <ForecastByCityName
                  direction='column'
                  onSubmitCityName={function (city) {
                    props.history.push({
                      pathname: '/forecast',
                      search: '?city=' + city
                    })
                  }}
                  cityName={123} />
              </div>
            )
          }} />

          <Route path='/forecast' component={Forecast} />

          <Route path='/details/:city' component={Detail} />
        </div>
      </BrowserRouter>
    )
  }
}

module.exports = App;